﻿CREATE TABLE [dbo].[InvoiceHeadstone] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                 INT             NULL,
    [Cemeterynumber]        INT             NULL,
    [Location]              NVARCHAR (2000) NULL,
    [Installdate]           DATETIME        NULL,
    [Funeraldirectornumber] INT             NULL,
    [Businessname]          NVARCHAR (2000) NULL,
    [Customernumber]        INT             NULL,
    [Name]                  NVARCHAR (2000) NULL,
    [Initials]              NVARCHAR (2000) NULL,
    [Invoiced]              BIT             NULL,
    [Invoicenumber]         INT             NULL,
    [Autonumberfield]       INT             NULL,
    [RegionCode]            NVARCHAR (5)    NULL,
    [fkCemetery]            INT             NULL,
    [fkFuneraldirector]     INT             NULL,
    [fkPlot]                INT             NULL,
    CONSTRAINT [nth_ihe_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



