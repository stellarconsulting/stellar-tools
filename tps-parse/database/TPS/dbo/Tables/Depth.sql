﻿CREATE TABLE [dbo].[Depth] (
    [ID]         INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]      INT             NULL,
    [Depth]      NVARCHAR (2000) NULL,
    [RegionCode] NVARCHAR (5)    NULL,
    CONSTRAINT [cen_dep_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



