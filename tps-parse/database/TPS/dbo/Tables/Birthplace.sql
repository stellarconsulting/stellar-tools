﻿CREATE TABLE [dbo].[Birthplace] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]        INT             NULL,
    [Placeofbirth] NVARCHAR (2000) NULL,
    [RegionCode]   NVARCHAR (5)    NULL,
    CONSTRAINT [cen_pob_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



