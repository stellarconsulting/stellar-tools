﻿CREATE TABLE [dbo].[Chapel] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]        INT             NULL,
    [Chapelnumber] INT             NULL,
    [Shortname]    NVARCHAR (2000) NULL,
    [Fullname]     NVARCHAR (2000) NULL,
    [Capacity]     INT             NULL,
    [RegionCode]   NVARCHAR (5)    NULL,
    CONSTRAINT [sth_chp_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



