﻿CREATE TABLE [dbo].[ImageFile] (
    [ID]                       INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                    INT             NULL,
    [Autonumberfield]          INT             NULL,
    [Imagefilename]            NVARCHAR (2000) NULL,
    [Imagefilenamenoextension] NVARCHAR (2000) NULL,
    [Imagefilenameextension]   NVARCHAR (2000) NULL,
    [Imagedate]                DATETIME        NULL,
    [Imageexportnotallowed]    BIT             NULL,
    [RegionCode]               NVARCHAR (5)    NULL,
    CONSTRAINT [sth_him_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



