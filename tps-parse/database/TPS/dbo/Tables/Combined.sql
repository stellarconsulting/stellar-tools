﻿CREATE TABLE [dbo].[Combined] (
    [ID]                      INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                   INT             NULL,
    [Autonumberfield]         INT             NULL,
    [Surname]                 NVARCHAR (2000) NULL,
    [Forenames]               NVARCHAR (2000) NULL,
    [Dateofburialorcremation] DATETIME        NULL,
    [Dateofdeath]             DATETIME        NULL,
    [Daterecordexported]      DATETIME        NULL,
    [Recordtype]              NVARCHAR (2000) NULL,
    [Parentfileid]            NVARCHAR (2000) NULL,
    [Parentautonumberfield]   INT             NULL,
    [RegionCode]              NVARCHAR (5)    NULL,
    CONSTRAINT [cen_com_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



