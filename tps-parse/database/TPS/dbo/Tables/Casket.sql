﻿CREATE TABLE [dbo].[Casket] (
    [ID]         INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]      INT             NULL,
    [Casketsize] NVARCHAR (2000) NULL,
    [RegionCode] NVARCHAR (5)    NULL,
    CONSTRAINT [cen_cas_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



