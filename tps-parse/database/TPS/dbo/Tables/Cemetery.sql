﻿CREATE TABLE [dbo].[Cemetery] (
    [ID]                         INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                      INT             NULL,
    [Cemeterynumber]             INT             NULL,
    [Cemeteryname]               NVARCHAR (2000) NULL,
    [Cemeteryabbreviation]       NVARCHAR (2000) NULL,
    [Cemeteryaddress]            NVARCHAR (2000) NULL,
    [Lastserialburials]          INT             NULL,
    [Lastserialcremations]       INT             NULL,
    [Lastregisterpageburials]    INT             NULL,
    [Lastregisterpagecremations] INT             NULL,
    [RegionCode]                 NVARCHAR (5)    NULL,
    CONSTRAINT [cen_cem_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



