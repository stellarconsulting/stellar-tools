﻿CREATE TABLE [dbo].[SubArea] (
    [ID]             INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]          INT             NULL,
    [Cemeterynumber] INT             NULL,
    [Areacode]       NVARCHAR (2000) NULL,
    [Subareacode]    NVARCHAR (2000) NULL,
    [Subareaname]    NVARCHAR (2000) NULL,
    [RegionCode]     NVARCHAR (5)    NULL,
    [fkCemetery]     INT             NULL,
    [fkArea]         INT             NULL,
    CONSTRAINT [cen_sub_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



