﻿CREATE TABLE [dbo].[FuneralDirector] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                 INT             NULL,
    [Funeraldirectornumber] INT             NULL,
    [Businessname]          NVARCHAR (2000) NULL,
    [Addressline1]          NVARCHAR (2000) NULL,
    [Addressline2]          NVARCHAR (2000) NULL,
    [Addressline3]          NVARCHAR (2000) NULL,
    [Town]                  NVARCHAR (2000) NULL,
    [Postcode]              NVARCHAR (2000) NULL,
    [Telephonenumber]       NVARCHAR (2000) NULL,
    [Faxnumber]             NVARCHAR (2000) NULL,
    [Contact1name]          NVARCHAR (2000) NULL,
    [Contact1phone]         NVARCHAR (2000) NULL,
    [Contact2name]          NVARCHAR (2000) NULL,
    [Contact2phone]         NVARCHAR (2000) NULL,
    [Debtorcode]            NVARCHAR (2000) NULL,
    [Inactive]              BIT             NULL,
    [Emailaddress]          NVARCHAR (2000) NULL,
    [Discountrate]          DECIMAL (10, 2) NULL,
    [Monumentalmasononly]   BIT             NULL,
    [Chargeaccount]         BIT             NULL,
    [RegionCode]            NVARCHAR (5)    NULL,
    CONSTRAINT [cen_fun_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



