﻿CREATE TABLE [dbo].[Ethnicity] (
    [ID]         INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]      INT             NULL,
    [Ethnicity]  NVARCHAR (2000) NULL,
    [RegionCode] NVARCHAR (5)    NULL,
    CONSTRAINT [cen_eth_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



