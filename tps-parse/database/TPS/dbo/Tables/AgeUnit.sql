﻿CREATE TABLE [dbo].[AgeUnit] (
    [ID]          INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]       INT             NULL,
    [Ageunit]     NVARCHAR (2000) NULL,
    [Agerequired] BIT             NULL,
    [RegionCode]  NVARCHAR (5)    NULL,
    CONSTRAINT [cen_age_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



