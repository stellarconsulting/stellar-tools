﻿CREATE TABLE [dbo].[Contact] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]           INT             NULL,
    [Contactnumber]   NVARCHAR (2000) NULL,
    [Name]            NVARCHAR (2000) NULL,
    [Title]           NVARCHAR (2000) NULL,
    [Initials]        NVARCHAR (2000) NULL,
    [Forenames]       NVARCHAR (2000) NULL,
    [Address1]        NVARCHAR (2000) NULL,
    [Address2]        NVARCHAR (2000) NULL,
    [Address3]        NVARCHAR (2000) NULL,
    [Town]            NVARCHAR (2000) NULL,
    [Postcode]        NVARCHAR (2000) NULL,
    [Telephonenumber] NVARCHAR (2000) NULL,
    [Faxnumber]       NVARCHAR (2000) NULL,
    [Emailaddress]    NVARCHAR (2000) NULL,
    [Contact1name]    NVARCHAR (2000) NULL,
    [Contact1phone]   NVARCHAR (2000) NULL,
    [Contact2name]    NVARCHAR (2000) NULL,
    [Contact2phone]   NVARCHAR (2000) NULL,
    [Category]        NVARCHAR (2000) NULL,
    [Alertuser]       NVARCHAR (2000) NULL,
    [Con_notes]       NVARCHAR (2000) NULL,
    [RegionCode]      NVARCHAR (5)    NULL,
    CONSTRAINT [cen_con_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



