﻿CREATE TABLE [dbo].[Area] (
    [ID]             INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]          INT             NULL,
    [Cemeterynumber] INT             NULL,
    [Areacode]       NVARCHAR (2000) NULL,
    [Areaname]       NVARCHAR (2000) NULL,
    [RegionCode]     NVARCHAR (5)    NULL,
    [fkCemetery]     INT             NULL,
    CONSTRAINT [cen_area_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



