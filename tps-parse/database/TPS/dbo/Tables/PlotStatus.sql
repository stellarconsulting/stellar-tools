﻿CREATE TABLE [dbo].[PlotStatus] (
    [ID]         INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]      INT             NULL,
    [Plotstatus] NVARCHAR (2000) NULL,
    [RegionCode] NVARCHAR (5)    NULL,
    CONSTRAINT [cen_pls_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



