﻿CREATE TABLE [dbo].[PaymentMethod] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]           INT             NULL,
    [Methodofpayment] NVARCHAR (2000) NULL,
    [RegionCode]      NVARCHAR (5)    NULL,
    CONSTRAINT [sth_pme_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



