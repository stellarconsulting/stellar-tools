﻿CREATE TABLE [dbo].[Headstone] (
    [ID]                     INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                  INT             NULL,
    [Autonumberfield]        INT             NULL,
    [Cemeterynumber]         INT             NULL,
    [Location]               NVARCHAR (2000) NULL,
    [Applicationdate]        DATETIME        NULL,
    [Permitnumber]           NVARCHAR (2000) NULL,
    [Permitdate]             DATETIME        NULL,
    [Installdate]            DATETIME        NULL,
    [Funeraldirectornumber]  INT             NULL,
    [Customernumber]         INT             NULL,
    [Chargeratecode]         NVARCHAR (2000) NULL,
    [Chargerateamount]       NVARCHAR (2000) NULL,
    [Chargeratedescription]  NVARCHAR (2000) NULL,
    [Chargeratedescription2] NVARCHAR (2000) NULL,
    [Invoiced]               BIT             NULL,
    [Invoicenumber]          INT             NULL,
    [Invoicedate]            DATETIME        NULL,
    [Stoporder]              BIT             NULL,
    [Hea_notes]              NVARCHAR (2000) NULL,
    [RegionCode]             NVARCHAR (5)    NULL,
    [fkCemetery]             INT             NULL,
    [fkPlot]                 INT             NULL,
    [fkFuneraldirector]      INT             NULL,
    CONSTRAINT [cen_hea_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



