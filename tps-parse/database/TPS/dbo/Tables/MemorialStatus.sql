﻿CREATE TABLE [dbo].[MemorialStatus] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]        INT             NULL,
    [Plaquestatus] NVARCHAR (2000) NULL,
    [RegionCode]   NVARCHAR (5)    NULL,
    CONSTRAINT [cen_mps_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



