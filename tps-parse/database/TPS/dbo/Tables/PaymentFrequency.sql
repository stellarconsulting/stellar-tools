﻿CREATE TABLE [dbo].[PaymentFrequency] (
    [ID]               INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]            INT             NULL,
    [Paymentfrequency] NVARCHAR (2000) NULL,
    [RegionCode]       NVARCHAR (5)    NULL,
    CONSTRAINT [sth_pfr_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



