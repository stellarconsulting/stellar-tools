﻿CREATE TABLE [dbo].[InvoiceExtra] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                 INT             NULL,
    [Extrasdate]            DATETIME        NULL,
    [Description1]          NVARCHAR (2000) NULL,
    [Description2]          NVARCHAR (2000) NULL,
    [Extrasrate]            INT             NULL,
    [Funeraldirectornumber] INT             NULL,
    [Businessname]          NVARCHAR (2000) NULL,
    [Customernumber]        INT             NULL,
    [Name]                  NVARCHAR (2000) NULL,
    [Initials]              NVARCHAR (2000) NULL,
    [Invoiced]              BIT             NULL,
    [Invoicenumber]         INT             NULL,
    [Autonumberfield]       INT             NULL,
    [RegionCode]            NVARCHAR (5)    NULL,
    [fkFuneraldirector]     INT             NULL,
    [fkCustomer]            INT             NULL,
    CONSTRAINT [cen_iex_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



