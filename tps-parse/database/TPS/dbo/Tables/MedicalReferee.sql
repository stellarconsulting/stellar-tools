﻿CREATE TABLE [dbo].[MedicalReferee] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                 INT             NULL,
    [Medicalrefereenumber]  INT             NULL,
    [Surname]               NVARCHAR (2000) NULL,
    [Title]                 NVARCHAR (2000) NULL,
    [Initials]              NVARCHAR (2000) NULL,
    [Address1]              NVARCHAR (2000) NULL,
    [Address2]              NVARCHAR (2000) NULL,
    [Address3]              NVARCHAR (2000) NULL,
    [Town]                  NVARCHAR (2000) NULL,
    [Postcode]              NVARCHAR (2000) NULL,
    [Telephone]             NVARCHAR (2000) NULL,
    [Fax]                   NVARCHAR (2000) NULL,
    [Gstregistered]         BIT             NULL,
    [Gstregistrationnumber] NVARCHAR (2000) NULL,
    [Inactive]              BIT             NULL,
    [RegionCode]            NVARCHAR (5)    NULL,
    CONSTRAINT [nth_med_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



