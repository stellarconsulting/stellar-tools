﻿CREATE TABLE [dbo].[Relation] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]           INT             NULL,
    [Relationship]    NVARCHAR (2000) NULL,
    [Autonumberfield] INT             NULL,
    [RegionCode]      NVARCHAR (5)    NULL,
    CONSTRAINT [cen_ria_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



