﻿CREATE TABLE [dbo].[District] (
    [ID]           INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]        INT             NULL,
    [Districtcode] NVARCHAR (2000) NULL,
    [Districtname] NVARCHAR (2000) NULL,
    [RegionCode]   NVARCHAR (5)    NULL,
    CONSTRAINT [cen_dis_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



