﻿CREATE TABLE [dbo].[InvoiceCremation] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                 INT             NULL,
    [Surname]               NVARCHAR (2000) NULL,
    [Forenames]             NVARCHAR (2000) NULL,
    [Dateofcremation]       DATETIME        NULL,
    [Funeraldirectornumber] INT             NULL,
    [Businessname]          NVARCHAR (2000) NULL,
    [Customernumber]        INT             NULL,
    [Name]                  NVARCHAR (2000) NULL,
    [Initials]              NVARCHAR (2000) NULL,
    [Invoiced]              BIT             NULL,
    [Invoicenumber]         INT             NULL,
    [Autonumberfield]       INT             NULL,
    [RegionCode]            NVARCHAR (5)    NULL,
    [fkCustomer]            INT             NULL,
    [fkFuneraldirector]     INT             NULL,
    CONSTRAINT [nth_icr_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



