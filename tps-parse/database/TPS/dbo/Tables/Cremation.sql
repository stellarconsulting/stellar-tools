﻿CREATE TABLE [dbo].[Cremation] (
    [ID]                        INT             IDENTITY (1, 1) NOT NULL,
    [Serialnumber]              INT             NULL,
    [Surname]                   NVARCHAR (2000) NULL,
    [Forenames]                 NVARCHAR (2000) NULL,
    [Gender]                    NVARCHAR (2000) NULL,
    [Age]                       INT             NULL,
    [Ageunit]                   NVARCHAR (2000) NULL,
    [Lastaddress]               NVARCHAR (2000) NULL,
    [Dateofdeath]               DATETIME        NULL,
    [Dateofcremation]           DATETIME        NULL,
    [Cemeterynumber]            INT             NULL,
    [Publish]                   BIT             NULL,
    [Cremationexportnotallowed] BIT             NULL,
    [Daterecordentered]         DATETIME        NULL,
    [Timerecordentered]         TIME (0)        NULL,
    [Cancelled]                 BIT             NULL,
    [Outofdistrict]             BIT             NULL,
    [Alertuser]                 BIT             NULL,
    [Invoiced]                  BIT             NULL,
    [Invoicenumber]             INT             NULL,
    [Invoicedate]               DATETIME        NULL,
    [Referenceno]               INT             NULL,
    [Chapelnumber]              INT             NULL,
    [Bookofremembrance]         BIT             NULL,
    [Servicestarttime]          TIME (0)        NULL,
    [Serviceendtime]            TIME (0)        NULL,
    [Funeraldirectornumber]     INT             NULL,
    [Medicalrefereenumber]      INT             NULL,
    [Medicalrefereedate]        DATETIME        NULL,
    [Dateashesavailable]        DATETIME        NULL,
    [Timeashesavailable]        TIME (0)        NULL,
    [Ashesdisposalmethod]       NVARCHAR (2000) NULL,
    [Ashesburialcemeterynumber] INT             NULL,
    [Ashesdisposaldate]         DATETIME        NULL,
    [Ashesburiallocation]       NVARCHAR (2000) NULL,
    [Customernumber]            INT             NULL,
    [Chargeratecode]            NVARCHAR (2000) NULL,
    [Chargerateamount]          INT             NULL,
    [Dateofdeathunknown]        BIT             NULL,
    [Dateofcremationunknown]    BIT             NULL,
    [Cremationdayname]          NVARCHAR (2000) NULL,
    [Casketsize]                NVARCHAR (2000) NULL,
    [Bookingtype]               NVARCHAR (2000) NULL,
    [Religion]                  NVARCHAR (2000) NULL,
    [Ethnicity]                 NVARCHAR (2000) NULL,
    [Placeofbirth]              NVARCHAR (2000) NULL,
    [Districtcode]              NVARCHAR (2000) NULL,
    [Occupation]                NVARCHAR (2000) NULL,
    [Dateofbirth]               DATETIME        NULL,
    [Dateofbirthunknown]        BIT             NULL,
    [Placeofdeath]              NVARCHAR (2000) NULL,
    [Applicant]                 NVARCHAR (2000) NULL,
    [Applicantrelationship]     NVARCHAR (2000) NULL,
    [RecNo]                     INT             NULL,
    [Cre_notesinternal]         NVARCHAR (2000) NULL,
    [Cre_notesexternal]         NVARCHAR (2000) NULL,
    [Autonumberfield]           INT             NULL,
    [RegionCode]                NVARCHAR (5)    NULL,
    [fkAgeunit]                 INT             NULL,
    [fkAshdisposal]             INT             NULL,
    [fkBookingtype]             INT             NULL,
    [fkAshesburialcemetery]     INT             NULL,
    [fkCemetery]                INT             NULL,
    [fkDistrict]                INT             NULL,
    [fkEthnicity]               INT             NULL,
    [fkFuneraldirector]         INT             NULL,
    [fkMedicalreferee]          INT             NULL,
    [fkPlot]                    INT             NULL,
    [fkChapel]                  INT             NULL,
    CONSTRAINT [nth_cre_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



