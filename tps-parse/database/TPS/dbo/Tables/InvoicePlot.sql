﻿CREATE TABLE [dbo].[InvoicePlot] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]           INT             NULL,
    [Cemeterynumber]  INT             NULL,
    [Areacode]        NVARCHAR (2000) NULL,
    [Subareacode]     NVARCHAR (2000) NULL,
    [Plotnumber]      NVARCHAR (2000) NULL,
    [Location]        NVARCHAR (2000) NULL,
    [Datereserved]    DATETIME        NULL,
    [Customernumber]  INT             NULL,
    [Name]            NVARCHAR (2000) NULL,
    [Initials]        NVARCHAR (2000) NULL,
    [Invoiced]        BIT             NULL,
    [Invoicenumber]   INT             NULL,
    [Autonumberfield] INT             NULL,
    [RegionCode]      NVARCHAR (5)    NULL,
    [fkCemetery]      INT             NULL,
    [fkPlot]          INT             NULL,
    CONSTRAINT [cen_ipl_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



