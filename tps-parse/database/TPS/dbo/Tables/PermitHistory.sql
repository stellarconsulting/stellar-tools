﻿CREATE TABLE [dbo].[PermitHistory] (
    [ID]                     INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                  INT             NULL,
    [Autonumberfield]        INT             NULL,
    [Applicationdate]        DATETIME        NULL,
    [Permitnumber]           NVARCHAR (2000) NULL,
    [Permitdate]             DATETIME        NULL,
    [Installdate]            DATETIME        NULL,
    [Funeraldirectornumber]  INT             NULL,
    [Customernumber]         INT             NULL,
    [Chargeratecode]         NVARCHAR (2000) NULL,
    [Chargerateamount]       INT             NULL,
    [Chargeratedescription]  NVARCHAR (2000) NULL,
    [Chargeratedescription2] NVARCHAR (2000) NULL,
    [Invoiced]               BIT             NULL,
    [Invoicenumber]          INT             NULL,
    [Invoicedate]            DATETIME        NULL,
    [Stoporder]              BIT             NULL,
    [RegionCode]             NVARCHAR (5)    NULL,
    [fkFuneraldirector]      INT             NULL,
    [fkChargerate]           INT             NULL,
    CONSTRAINT [cen_per_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



