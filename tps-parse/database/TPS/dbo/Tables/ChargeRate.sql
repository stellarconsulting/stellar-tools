﻿CREATE TABLE [dbo].[ChargeRate] (
    [ID]                     INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                  INT             NULL,
    [Chargeratecode]         NVARCHAR (2000) NULL,
    [Chargeratedescription]  NVARCHAR (2000) NULL,
    [Chargeratedescription2] NVARCHAR (2000) NULL,
    [Chargerate]             INT             NULL,
    [Secondchargerate]       INT             NULL,
    [Ledgercode]             NVARCHAR (2000) NULL,
    [Secondledgercode]       NVARCHAR (2000) NULL,
    [Chargeratecategory]     NVARCHAR (2000) NULL,
    [Obsolete]               BIT             NULL,
    [RegionCode]             NVARCHAR (5)    NULL,
    CONSTRAINT [cen_cha_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



