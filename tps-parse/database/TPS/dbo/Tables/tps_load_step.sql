﻿CREATE TABLE [dbo].[tps_load_step] (
    [ID]        INT           IDENTITY (1, 1) NOT NULL,
    [step]      VARCHAR (250) NULL,
    [step_time] DATETIME      NULL
);

