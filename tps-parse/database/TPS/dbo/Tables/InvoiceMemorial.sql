﻿CREATE TABLE [dbo].[InvoiceMemorial] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                 INT             NULL,
    [Cemeterynumber]        INT             NULL,
    [Location]              NVARCHAR (2000) NULL,
    [Surname1]              NVARCHAR (2000) NULL,
    [Forenames1]            NVARCHAR (2000) NULL,
    [Surname2]              NVARCHAR (2000) NULL,
    [Forenames2]            NVARCHAR (2000) NULL,
    [Actiondate]            DATETIME        NULL,
    [Funeraldirectornumber] INT             NULL,
    [Businessname]          NVARCHAR (2000) NULL,
    [Customernumber]        INT             NULL,
    [Name]                  NVARCHAR (2000) NULL,
    [Initials]              NVARCHAR (2000) NULL,
    [Invoiced]              BIT             NULL,
    [Invoicenumber]         INT             NULL,
    [Autonumberfield]       INT             NULL,
    [RegionCode]            NVARCHAR (5)    NULL,
    [fkPlot]                INT             NULL,
    [fkCemetery]            INT             NULL,
    CONSTRAINT [nth_ime_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



