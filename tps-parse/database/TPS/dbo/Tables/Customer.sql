﻿CREATE TABLE [dbo].[Customer] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]           INT             NULL,
    [Customernumber]  INT             NULL,
    [Name]            NVARCHAR (2000) NULL,
    [Title]           NVARCHAR (2000) NULL,
    [Initials]        NVARCHAR (2000) NULL,
    [Forenames]       NVARCHAR (2000) NULL,
    [Address1]        NVARCHAR (2000) NULL,
    [Address2]        NVARCHAR (2000) NULL,
    [Address3]        NVARCHAR (2000) NULL,
    [Town]            NVARCHAR (2000) NULL,
    [Postcode]        NVARCHAR (2000) NULL,
    [Debtorcode]      NVARCHAR (2000) NULL,
    [Telephonenumber] NVARCHAR (2000) NULL,
    [Districtcode]    NVARCHAR (2000) NULL,
    [Category]        NVARCHAR (2000) NULL,
    [Alertuser]       BIT             NULL,
    [Cus_notes]       NVARCHAR (2000) NULL,
    [RegionCode]      NVARCHAR (5)    NULL,
    [fkDistrict]      INT             NULL,
    CONSTRAINT [cen_cus_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



