﻿CREATE TABLE [dbo].[Religion] (
    [ID]         INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]      INT             NULL,
    [Religion]   NVARCHAR (2000) NULL,
    [RegionCode] NVARCHAR (5)    NULL,
    CONSTRAINT [cen_rel_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



