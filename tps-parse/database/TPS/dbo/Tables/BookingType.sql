﻿CREATE TABLE [dbo].[BookingType] (
    [ID]                              INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                           INT             NULL,
    [Bookingtype]                     NVARCHAR (2000) NULL,
    [Durationallowed]                 INT             NULL,
    [Nofaciltyrequired]               BIT             NULL,
    [Actionifnoservicetime]           NVARCHAR (2000) NULL,
    [Excludefromwebexport]            BIT             NULL,
    [Excludefrominvoicingdescription] BIT             NULL,
    [Notacremation]                   BIT             NULL,
    [Defaultchapelonly]               BIT             NULL,
    [Statscolumnorder]                INT             NULL,
    [RegionCode]                      NVARCHAR (5)    NULL,
    CONSTRAINT [cen_book_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



