﻿CREATE TABLE [dbo].[InvoiceBurial] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                 INT             NULL,
    [Cemeterynumber]        INT             NULL,
    [Surname]               NVARCHAR (2000) NULL,
    [Forenames]             NVARCHAR (2000) NULL,
    [Dateofburial]          DATETIME        NULL,
    [Funeraldirectornumber] INT             NULL,
    [Businessname]          NVARCHAR (2000) NULL,
    [Customernumber]        INT             NULL,
    [Name]                  NVARCHAR (2000) NULL,
    [Initials]              NVARCHAR (2000) NULL,
    [Invoiced]              BIT             NULL,
    [Invoicenumber]         INT             NULL,
    [Autonumberfield]       INT             NULL,
    [RegionCode]            NVARCHAR (5)    NULL,
    [fkCemetery]            INT             NULL,
    CONSTRAINT [cen_ibu_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



