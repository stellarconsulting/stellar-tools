﻿CREATE TABLE [dbo].[ExtraMisc] (
    [ID]                    INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]                 INT             NULL,
    [Associatedentrytype]   NVARCHAR (2000) NULL,
    [Cemeterynumber]        INT             NULL,
    [Serialnumber]          INT             NULL,
    [Dateentered]           DATETIME        NULL,
    [Dateamended]           DATETIME        NULL,
    [Extrasdate]            DATETIME        NULL,
    [Quantity]              INT             NULL,
    [Description1]          NVARCHAR (2000) NULL,
    [Description2]          NVARCHAR (2000) NULL,
    [Chargeratecode]        NVARCHAR (2000) NULL,
    [Extrasrate]            INT             NULL,
    [Ratenotstandard]       BIT             NULL,
    [Extrasamount]          DECIMAL (18, 2) NULL,
    [Funeraldirectornumber] INT             NULL,
    [Customernumber]        INT             NULL,
    [Invoiced]              BIT             NULL,
    [Invoicenumber]         INT             NULL,
    [Invoicedate]           DATETIME        NULL,
    [Cancelled]             BIT             NULL,
    [Autonumberfield]       INT             NULL,
    [Parentautonumberfield] INT             NULL,
    [RegionCode]            NVARCHAR (5)    NULL,
    [fkCemetery]            INT             NULL,
    [fkChargerate]          INT             NULL,
    [fkFuneraldirector]     INT             NULL,
    CONSTRAINT [cen_exm_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



