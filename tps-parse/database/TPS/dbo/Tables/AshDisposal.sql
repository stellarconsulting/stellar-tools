﻿CREATE TABLE [dbo].[AshDisposal] (
    [ID]                  INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]               INT             NULL,
    [Ashesdisposalmethod] NVARCHAR (2000) NULL,
    [RegionCode]          NVARCHAR (5)    NULL,
    CONSTRAINT [cen_ash_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



