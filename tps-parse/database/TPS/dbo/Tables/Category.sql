﻿CREATE TABLE [dbo].[Category] (
    [ID]         INT             IDENTITY (1, 1) NOT NULL,
    [RecNo]      INT             NULL,
    [Category]   NVARCHAR (2000) NULL,
    [RegionCode] NVARCHAR (5)    NULL,
    CONSTRAINT [cen_cat_mig_PK_PK] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);



