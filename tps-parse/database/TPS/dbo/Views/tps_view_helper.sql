﻿
CREATE view [dbo].[tps_view_helper] as
with tbs as
(
select 
v.name as table_name
,vc.name as table_col_name
,vc.column_id as table_column_id
 from sys.tables v
inner join sys.columns vc on v.object_id = vc.object_id
where v.name IN
(select target_table from [dbo].[tps_view_master_tables]
)

)
, vws as
(
select 
v.name as view_name
,vc.name as view_col_name
 from sys.views v
inner join sys.columns vc on v.object_id = vc.object_id
where v.name like 'view_tps[_]%'
)
select 
top 10000
* from tbs t
left join vws v on 'view_tps_'+t.table_name = v.view_name
		and t.table_col_name = v.view_col_name
where t.table_col_name <> 'ID' and t.table_col_name not like 'fk%'
order by v.view_name,t.table_name,t.table_column_id