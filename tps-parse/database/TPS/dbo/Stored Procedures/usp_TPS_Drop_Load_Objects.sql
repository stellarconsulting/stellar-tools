﻿CREATE PROC [dbo].[usp_TPS_Drop_Load_Objects] AS

declare @query varchar(max) = ''

select @query = @query + char(10)+'drop table dbo.'+name from sys.tables where 
name like 'tps[_]cen[_]%[_]mig'
or name like 'tps[_]nth[_]%[_]mig'
or name like 'tps[_]sth[_]%[_]mig'

exec(@query)
set @query = ''

select @query = @query + char(10)+'drop view dbo.'+name from sys.views where 
name like 'vw_tps[_]%[_]mig'
or name like 'vw_tps[_]%[_]mig'
or name like 'vw_tps[_]%[_]mig'
or name like 'view[_]tps[_]%'
or name = 'tps_load_step'

exec(@query)
set @query = ''

select @query = @query + char(10)+'truncate table dbo.'+name from sys.tables where name in
(select "target_table" from
 tps_view_master_tables
 a )

exec(@query)
set @query = ''