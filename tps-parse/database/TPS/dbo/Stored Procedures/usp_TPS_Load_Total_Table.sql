﻿CREATE proc [dbo].[usp_TPS_Load_Total_Table] as
if(object_id('tps_total_table') IS NOT NULL) DROP TABLE tps_total_table;
create table tps_total_table(obj_type varchar(150), obj_name varchar(150),regioncode varchar(20), rs bigint);


if(object_id('tempdb..#exec') IS NOT NULL) DROP TABLE #exec;
create table #exec(ID INT IDENTITY(1,1),query varchar(max));

insert into #exec
select 
query = 'insert into tps_total_table '+ query + char(10) 
 from 
(
select 'select obj_type=''Final Table Copy'',obj_name='''+t.source_view+''',regioncode,count(1) as rs from '+tb.name+' group by regioncode' as query
from [dbo].[tps_view_master_tables] t
inner join sys.tables tb on t.target_table+'_tmp' = tb.name
UNION 
select 'select obj_type=''Final Table'',obj_name='''+t.source_view+''',regioncode,count(1) as rs from '+t.target_table+' group by regioncode' as query
from [dbo].[tps_view_master_tables] t
inner join sys.tables tb on t.target_table = tb.name
UNION ALL
select 'select obj_type=''Final Table All'',obj_name='''+t.source_view+''',regioncode=''ALL'',-1*count(1) as rs from '+t.target_table+'' as query
from [dbo].[tps_view_master_tables] t
inner join sys.tables tb on t.target_table = tb.name
UNION ALL
select 'select obj_type=''Merge View'',obj_name='''+t.source_view+''',regioncode=UPPER(right(left(region_code,5),1)),count(1) as rs from '+v.name+' group by UPPER(right(left(region_code,5),1))' as query
 from  [dbo].[tps_view_master_tables] t
inner join sys.views v on 'vw_tps_'+t.source_view = v.name
UNION ALL
select 'select obj_type=''Merge View All'',obj_name='''+t.source_view+''',regioncode=''ALL'',-1*count(1) as rs from '+v.name as query
 from  [dbo].[tps_view_master_tables] t
inner join sys.views v on 'vw_tps_'+t.source_view = v.name
UNION ALL
select 'select obj_type=''Load Table'',obj_name='''+t.source_view+''',regioncode='''+UPPER(right(left(tb.name,5),1))+''',count(1) as rs from '+tb.name as query
from [dbo].[tps_view_master_tables] t
inner join sys.tables tb on tb.name like 'tps[_]___[_]'+t.source_view
UNION ALL
select 'select obj_type=''Load Table All'',obj_name='''+t.source_view+''',regioncode=''ALL'',-1*count(1) as rs from '+tb.name+' ' as query
from [dbo].[tps_view_master_tables] t
inner join sys.tables tb on tb.name like 'tps[_]___[_]'+t.source_view
UNION ALL
select 'select obj_type=''CSV File Raw'',obj_name='''+t.source_view+''',regioncode='''+UPPER(right(left(tb.name,5),1))+''',-1*count(1) as rs from '+tb.name+' ' as query
from [dbo].[tps_view_master_tables] t
inner join sys.tables tb on tb.name like 'tps[_]___[_]'+t.source_view
) a



declare @query varchar(max)=''


declare @eid int = 1 ,@emaxid int = (select max(id) from #exec)

set @QUERY = ''

set nocount off

while @eid <= @emaxid
begin
	
	select @QUERY =  query from #exec where id = @eid
	
	PRINT(@query)
	EXEC(@query)
	SET @query = ''
	
	set @eid = @eid + 1
end



select * from tps_total_table