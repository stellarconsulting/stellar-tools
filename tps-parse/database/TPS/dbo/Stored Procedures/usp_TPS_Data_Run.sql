﻿CREATE PROC [dbo].[usp_TPS_Data_Run] AS

exec [dbo].[usp_TPS_Create_Merge_Views]
exec [dbo].[usp_TPS_Create_Insert_Views]
exec [dbo].[usp_TPS_Create_Insert_Scripts]