﻿CREATE proc [dbo].[usp_TPS_Create_Insert_Views] AS
set nocount on

if object_id('tempdb..#t') is not null drop table #t

if(object_id('tempdb..#exec') IS NOT NULL) DROP TABLE #exec;
create table #exec(ID INT IDENTITY(1,1),query varchar(max));


select top 10000
t.name as table_name
,vm.source_view
,c.name as column_name
,c.column_id
,y.name
,id = row_number() OVER(ORDER BY t.name,c.column_id)
,transform =case when row_number() over(partition by t.name order by c.column_id) = 1 then  'GO'+char(10)+'if object_id(''view_tps_'+t.name+''') IS NOT NULL drop view view_tps_'+t.name+char(10)+'GO'+char(10)+'CREATE VIEW view_tps_'+t.name+' AS'+char(10)+' SELECT '+char(10)
			else '' end+
			case when column_id <> 2 then ',' else '' end+QUOTENAME(c.name)+' = '+
			replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
			case y.name
			when 'nvarchar' then 
				case when c.name <> 'RegionCode' then 'CONVERT(nvarchar('+convert(varchar,(c.max_length/2))+'),[dbo].[ufn_tps_ClearText]('+quotename(c.name)+'))'
				else 'CONVERT(nvarchar('+convert(varchar,(c.max_length/2))+'),[dbo].[ufn_tps_GetRegion]([dbo].[ufn_tps_ClearText]('+quotename(c.name)+')))' end
			when 'int' then '[dbo].[ufn_tps_ConvertToNumber]([dbo].[ufn_tps_ClearText]('+quotename(c.name)+'))'
			when 'bit' then 'dbo.ufn_tps_ConvertToBoolean([dbo].[ufn_tps_ClearText]('+quotename(c.name)+'))'
			when 'datetime' then '[dbo].[ufn_tps_ConvertToDateTime]([dbo].[ufn_tps_ClearText](('+quotename(c.name)+')))'
			when 'time' then '[dbo].[ufn_tps_ConvertToTime]([dbo].[ufn_tps_ClearText](('+quotename(c.name)+')))'
			when 'decimal' then '[dbo].[ufn_tps_ConvertToDecimal]([dbo].[ufn_tps_ClearText]('+quotename(c.name)+'))'
			else null end
				,'RecNo', 'Rec No')
				,'RegionCode','region_code')
				,'Bur_notesinternal','Bur:notesinternal')
				,'Bur_notesexternal','Bur:notesexternal')
				,'Con_Notes','Con:notes')
				,'Cre_notesinternal','Cre:notesinternal')
				,'Cre_notesexternal','Cre:notesexternal')
				,'Cus_notes','Cus:notes')
				,'Hea_notes','Hea:notes')
				,'Mem_notes','Mem:notes')
				,'Plo_notes','Plo:notes')
			+case when row_number() over(partition by t.name order by c.column_id desc) = 1 then char(10)+'FROM vw_tps_'+vm.source_view+char(10) else '' end
into #t
from 
sys.tables t
inner join sys.columns c on t.object_id = c.object_id
inner join sys.types y on c.system_type_id = y.system_type_id
	 and y.name <> 'sysname'
inner JOIN
tps_view_master_tables
vm on t.name = vm.target_table
where t.type_desc = 'user_table'
and t.name not like 'tps%'
and c.is_identity = 0
and left(c.name,2) <> 'fk'
and OBJECT_ID('vw_tps_'+vm.source_view) IS NOT NULL
order by t.name,c.column_id

declare @query varchar(max)

declare @id INT = 1,@maxid int = (select max(id) from #t)

while @id <= @maxid
begin
	select @query --= case when table_name = 'InvoiceLine' THEN REPLACE(transform,'([Bookingtype])','([Typeofservice])') ELSE transform END  
					= transform from #t where id = @id
	INSERT INTO #exec select Item from [dbo].[usfn_tps_SplitRows](@QUERY,char(10))
	set @id = @id + 1
end

INSERT INTO #exec select Item from [dbo].[usfn_tps_SplitRows]('GO',char(10))


declare @eid int = 1 ,@emaxid int = (select max(id) from #exec),@goquery varchar(max) = 'GO'

set @QUERY = ''

while @eid <= @emaxid
begin
	
	select @QUERY = @QUERY+ CASE WHEN query = 'GO' THEN '' ELSE query END + char(10),@goquery = query from #exec where id = @eid
	
	if(@goquery = 'GO' and @QUERY <> 'GO') 
	BEGIN
		PRINT(@query)		
		EXEC(@query)
		SET @query = ''
	END
	set @eid = @eid + 1
end