﻿CREATE proc [dbo].[usp_TPS_Create_Merge_Views] AS
set nocount on;
if(object_id('tempdb..#vw_tps_script') IS NOT NULL) DROP TABLE #vw_tps_script;
if(object_id('tempdb..#exec') IS NOT NULL) DROP TABLE #exec;
create table #exec(ID INT IDENTITY(1,1),query varchar(max));
with all_cols as
(
select 
distinct replace(replace(replace(t.name,'tps_cen_',''),'tps_nth_',''),'tps_sth_','') as merge_view_name,
quotename(replace(c.name,'"','')) as viewcol_name
,quotename(c.name) as srccol_name
,left(t.name,8) as src_table
,c.column_id
 from sys.tables  t
inner join sys.columns c on t.object_id = c.object_id
where t.name like 'tps[_]%' and OBJECT_SCHEMA_NAME(t.object_id) = 'dbo'
)
,src_tab as
(
SELECT
'tps_nth_' as  "src_table"
  UNION ALL  SELECT
'tps_sth_' as  "src_table"
  UNION ALL  SELECT
'tps_cen_' as "src_table" 
)
,col_merge_01 as
(
select 
merge_view_name,viewcol_name,min(column_id) as column_id 
from all_cols
group by merge_view_name,viewcol_name
)
,col_merge as
(
select merge_view_name,viewcol_name
,row_number() over(partition by merge_view_name order by column_id,viewcol_name) as column_id  
,row_number() over(partition by merge_view_name order by column_id desc,viewcol_name desc) as last_column_id  
from col_merge_01
)
,src_tables as 
(
select 
sc.src_table,merge_view_name,src_id = ROW_NUMBER() OVER (PARTITION BY cm.merge_view_name ORDER BY sc.src_table)
from src_tab sc 
INNER JOIN (select distinct merge_view_name from col_merge) cm on 1=1
where OBJECT_ID(sc.src_table+cm.merge_view_name) IS NOT NULL
)

select 
cm.merge_view_name,sc.src_id,cm.column_id,
view_group_id = DENSE_RANK() OVER(ORDER BY cm.merge_view_name),
view_create_id = ROW_NUMBER() OVER(PARTITION BY cm.merge_view_name ORDER BY sc.src_id,cm.column_id),
query_script = case when cm.column_id = 1 and sc.src_id = 1 THEN 'GO'+char(10)+'if object_id(''vw_tps_'+cm.merge_view_name+''') IS NOT NULL DROP VIEW vw_tps_'+cm.merge_view_name+char(10)+'GO'+char(10)
	+'CREATE VIEW vw_tps_'+cm.merge_view_name+' as '+char(10) ELSE '' END
+case when cm.column_id = 1 THEN
	case when  sc.src_id = 1 THEN '' ELSE 'UNION ALL'+char(10) END
	+'SELECT region_code = '''+sc.src_table+''','+cm.viewcol_name+' = '+CASE WHEN ac.merge_view_name IS NOT NULL THEN ac.srccol_name ELSE 'CAST(null as varchar(max))' END
else ','+cm.viewcol_name+' = '+CASE WHEN ac.merge_view_name IS NOT NULL THEN ac.srccol_name ELSE 'CAST(null as varchar(max))' END END
+case when cm.last_column_id  = 1 THEN CHAR(10)+'from '+sc.src_table+cm.merge_view_name+char(10) ELSE '' END
into #vw_tps_script
 from col_merge cm
inner join src_tab sc1 on 1=1
inner join src_tables sc on  sc1.src_table+cm.merge_view_name = sc.src_table+sc.merge_view_name
left join all_cols ac on cm.merge_view_name = ac.merge_view_name
	and cm.viewcol_name = ac.viewcol_name
	and sc.src_table = ac.src_table


declare @id int = 1 ,@maxid int = (select max(view_group_id) from #vw_tps_script)



while @id <= @maxid begin
	
	
	declare @query varchar(max) = ''
	declare @innerid int = 1 ,@innermaxid int = (select max(view_create_id) from #vw_tps_script where view_group_id = @id)
	
	while @innerid <= @innermaxid
	begin
		select  @query = @query + query_script from #vw_tps_script where view_group_id = @id and view_create_id = @innerid
		set @innerid = @innerid + 1

		


	end
	
	
	INSERT INTO #exec select Item from [dbo].[usfn_tps_SplitRows](@QUERY,char(10))
	
	

	
	
	
	set @id = @id + 1
end

INSERT INTO #exec select Item from [dbo].[usfn_tps_SplitRows]('GO',char(10))


declare @eid int = 1 ,@emaxid int = (select max(id) from #exec),@goquery varchar(max) = 'GO'

set @QUERY = ''

while @eid <= @emaxid
begin
	
	select @QUERY = @QUERY+ CASE WHEN query = 'GO' THEN '' ELSE query END + char(10),@goquery = query from #exec where id = @eid
	
	if(@goquery = 'GO' and @QUERY <> 'GO') 
	BEGIN
		
		EXEC(@query)
		SET @query = ''
	END
	set @eid = @eid + 1
end