﻿CREATE proc [dbo].[usp_TPS_Create_Insert_Scripts] AS
set nocount on

if object_id('tempdb..#t') is not null drop table #t

if(object_id('tempdb..#exec') IS NOT NULL) DROP TABLE #exec;
create table #exec(ID INT IDENTITY(1,1),query varchar(max));


if(object_id('tps_load_step') IS NOT NULL) DROP TABLE tps_load_step;
create table tps_load_step(ID INT IDENTITY(1,1),step varchar(250),step_time datetime);


select top 10000
t.name as table_name
,vm.source_view
,c.name as column_name
,c.column_id
,y.name
,id = row_number() OVER(ORDER BY t.name,c.column_id)
,last_id = row_number() OVER(partition by  t.name ORDER BY c.column_id desc)
,transform =case when row_number() over(partition by t.name order by c.column_id) = 1 then  'GO'+char(10)+'truncate table '+QUOTENAME(t.name)+char(10)+'GO'+char(10)+'INSERT INTO tps_load_step select ''Loading:'+QUOTENAME(t.name)+''',GETDATE()'+CHAR(10)+'GO'+CHAR(10)+'INSERT INTO '+QUOTENAME(t.name)+' ( '+char(10)
			else '' end+
			case when column_id <> 2 then ',' else '' end+QUOTENAME(c.name)
			+case when row_number() over(partition by t.name order by c.column_id desc) = 1 then char(10)+')' else '' end
,from_cols =case when row_number() over(partition by t.name order by c.column_id) = 1 then  'SELECT '+char(10)
			else '' end+
			case when column_id <> 2 then ',' else '' end+QUOTENAME(c.name)
			+case when row_number() over(partition by t.name order by c.column_id desc) = 1 then char(10)+'' else '' end

				
,from_stmt = case when row_number() over(partition by t.name order by c.column_id desc) = 1 then char(10)+'FROM '+QUOTENAME('view_tps_'+t.name)+'GO'+char(10)+'INSERT INTO tps_load_step select ''Done:'+QUOTENAME(t.name)+''',GETDATE()'+CHAR(10)+'GO' else '' end
into #t
from 
sys.tables t
inner join sys.columns c on t.object_id = c.object_id
inner join sys.types y on c.system_type_id = y.system_type_id
	 and y.name <> 'sysname'
inner JOIN dbo.tps_view_master_tables vm on t.name = vm.target_table
where t.type_desc = 'user_table'
and t.name not like 'tps%'
and c.is_identity = 0
and left(c.name,2) <> 'fk'
and OBJECT_ID(QUOTENAME('view_tps_'+t.name)) IS NOT NULL
order by t.name,c.column_id

declare @query varchar(max)='',@from_cols varchar(max)='',@from_stmt varchar(max)=''


declare @id INT = 1,@maxid int = (select max(id) from #t),@last_id int = 0

while @id <= @maxid
begin
	
	select @query =  @query+transform,@last_id = last_id,@from_cols = @from_cols + from_cols,@from_stmt = @from_stmt+from_stmt  from #t where id = @id
	
	if(@last_id=1)
	BEGIN
		

		INSERT INTO #exec select Item from [dbo].[usfn_tps_SplitRows](@QUERY,char(10))
		INSERT INTO #exec select Item from [dbo].[usfn_tps_SplitRows](@from_cols,char(10))
		INSERT INTO #exec select Item from [dbo].[usfn_tps_SplitRows](@from_stmt,char(10))
		
		set @QUERY =  ''
		set @from_cols =  ''
		set @from_stmt =  ''
	END
	
	set @id = @id + 1
end


INSERT INTO #exec select Item from [dbo].[usfn_tps_SplitRows]('GO',char(10))


declare @eid int = 1 ,@emaxid int = (select max(id) from #exec),@goquery varchar(max) = 'GO'

set @QUERY = ''

set nocount off

while @eid <= @emaxid
begin
	
	select @QUERY = @QUERY+ CASE WHEN query = 'GO' THEN '' ELSE query END + char(10),@goquery = query from #exec where id = @eid
	
	if(@goquery = 'GO' and @QUERY <> 'GO') 
	BEGIN
		PRINT(@query)
		EXEC(@query)
		SET @query = ''
	END
	set @eid = @eid + 1
end

