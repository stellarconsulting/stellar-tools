﻿



CREATE FUNCTION [dbo].[ufn_tps_ConvertToNumber]
(
	@param1 nvarchar(max)
)
RETURNS int
AS
BEGIN
	

	-- Return the result of the function
	RETURN CONVERT(INT,CONVERT(float, NULLIF(dbo.ufn_tps_RemoveChars([dbo].[ufn_tps_ClearText](LTRIM(RTRIM(@param1)))),'')))

END