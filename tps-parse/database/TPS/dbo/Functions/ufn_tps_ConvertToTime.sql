﻿




CREATE FUNCTION [dbo].[ufn_tps_ConvertToTime]
(
	@param1 nvarchar(max)
)
RETURNS TIME
AS
BEGIN
	-- Return the result of the function 
	RETURN convert(time,convert(datetime, dateadd(ms, ISNULL((CONVERT(BIGINT,@param1)-1),0)*10,0)))
END