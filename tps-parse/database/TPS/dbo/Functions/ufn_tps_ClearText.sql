﻿


CREATE FUNCTION [dbo].[ufn_tps_ClearText]
(
	@param1 nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN
	
	declare @p1 nvarchar(max) = REPLACE(REPLACE(REPLACE(@param1,'""','"'),'^newline^',char(18)+char(17)),'^tilde^','~'),@p nvarchar(max) = ''

	if(len(@p1) > 3 and left(@p1,1) = '"' and right(@p1,1) = '"'  )
		set @p = SUBSTRING(@p1,2,lEN(@p1)-2)
	else set  @p = replace(@p1,'"','')
	-- Return the result of the function
	RETURN LTRIM(RTRIM(@p))

END