﻿



CREATE FUNCTION [dbo].[ufn_tps_ConvertToDateTime]
(
	@param1 nvarchar(max)
)
RETURNS DATETIME
AS
BEGIN
	
	-- Return the result of the function 
	RETURN ISNULL(DateAdd(day, NULLIF(CONVERT(INT,@param1),0)  - 4, '1801-01-01') ,'1899-12-31')

END