﻿

CREATE FUNCTION [dbo].[ufn_tps_GetRegion]
(
	@param1 nvarchar(8)
)
RETURNS nvarchar(5)
AS
BEGIN
	

	-- Return the result of the function
	RETURN  CASE @param1 WHEN 'tps_nth_' THEN  'N' WHEN 'tps_sth_' THEN 'S' WHEN 'tps_cen_' THEN 'C' END

END