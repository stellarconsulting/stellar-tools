﻿




CREATE FUNCTION [dbo].[ufn_tps_ConvertToDecimal]
(
	@param1 nvarchar(max)
)
RETURNS float
AS
BEGIN
	

	-- Return the result of the function
	RETURN CONVERT(float,CONVERT(float, NULLIF(dbo.ufn_tps_RemoveChars(@param1),'')))

END