﻿


CREATE FUNCTION [dbo].[ufn_tps_ConvertToBoolean]
(
	@param1 nvarchar(max)
)
RETURNS bit
AS
BEGIN
	

	-- Return the result of the function
	RETURN CONVERT(bit, ISNULL(@param1,0))

END