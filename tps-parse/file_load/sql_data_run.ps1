param( $sqlserver = "localhost" , $database = "TPS" )


$connectionstring = "Data Source=$sqlserver;Integrated Security=true;Initial Catalog=$database;Connection Timeout=300000" 

function RunSQL($SQL){
    $SqlConnection = New-Object System.Data.SqlClient.SqlConnection
    $SqlConnection.ConnectionString = $ConnectionString
    
    $SqlConnection.Open()
    $command = $SqlConnection.CreateCommand()
    $command.CommandTimeout = 0
    $command.CommandText = $SQL
    #$command
    $command.ExecuteNonQuery()
    $SqlConnection.Close()
}

RunSQL "exec [dbo].[usp_TPS_Data_Run]"
RunSQL "[dbo].[usp_TPS_Load_Total_Table]"
$cmd =  "sqlcmd -S "+$sqlserver+" -d "+$database+" -s, -W -Q ""SET NOCOUNT ON ;SELECT * FROM dbo.tps_total_table"" > logs/tps_total_table.csv"
cmd.exe /c $cmd 

$csv_make = Select-String -Path "LOGS/csv_make_rows.txt" -Pattern "CSV File,"
$tps_total_csv = Get-Content -Path "LOGS/tps_total_table.csv" Where-Object {$_ -notmatch '--------,'}
Set-Content "logs/tps_total_table.csv" $tps_total_csv
foreach($i in $csv_make){
    $line = $i.Line.tostring().replace("out\sth_","").replace("out\cen_","").replace("out\nth_","")
    Add-Content "logs/tps_total_table.csv"  $line
}