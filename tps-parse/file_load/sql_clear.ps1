param( $sqlserver = "localhost" , $database = "TPS" )


$connectionstring = "Data Source=$sqlserver;Integrated Security=true;Initial Catalog=$database;" 

function RunSQL($SQL){
    $SqlConnection = New-Object System.Data.SqlClient.SqlConnection
    $SqlConnection.ConnectionString = $ConnectionString
    
    $SqlConnection.Open()
    $command = $SqlConnection.CreateCommand()
    $command.CommandText = $SQL
    $null = $command.ExecuteNonQuery()
    $SqlConnection.Close()
}

RunSQL "exec [dbo].[usp_TPS_Drop_Load_Objects]"