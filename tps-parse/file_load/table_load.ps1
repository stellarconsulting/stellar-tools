param($delfile = "cen_bur_mig", $sqlserver = "localhost" , $database = "TPS" )

#################################################### 
# 
# PowerShell CSV to SQL Import Script 
# 
#################################################### 
 
# Database variables 



$table = "tps_"+$delfile
$infile = "out\"+$delfile

IF (-Not(Test-Path $infile)){
    Write-Host $infile+ "FILE NOT FOUND!"
    Exit

}

  
# CSV variables 
$scriptDir = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent


$csvfile = $scriptDir+"\"+$infile
$csvfile
$csvdelimiter = "~" 
$FirstRowColumnNames = $true 

$connectionstring = "Data Source=$sqlserver;Integrated Security=true;Initial Catalog=$database;" 

function RunSQL($SQL){
    $SqlConnection = New-Object System.Data.SqlClient.SqlConnection
    $SqlConnection.ConnectionString = $ConnectionString
    
    $SqlConnection.Open()
    $command = $SqlConnection.CreateCommand()
    $command.CommandText = $SQL
    $null = $command.ExecuteNonQuery()
    $SqlConnection.Close()
}
  
################### No need to modify anything below ################### 
Write-Host "Script started..." 
$elapsed = [System.Diagnostics.Stopwatch]::StartNew()  
[void][Reflection.Assembly]::LoadWithPartialName("System.Data") 
[void][Reflection.Assembly]::LoadWithPartialName("System.Text.Encoding") 
[void][Reflection.Assembly]::LoadWithPartialName("System.Data.SqlClient") 
  
# 50k worked fastest and kept memory usage to a minimum 
$batchsize = 50000 
  
# Build the sqlbulkcopy connection, and set the timeout to infinite 

$bulkcopy = New-Object Data.SqlClient.SqlBulkCopy($connectionstring, [System.Data.SqlClient.SqlBulkCopyOptions]::TableLock) 
$bulkcopy.DestinationTableName = $table 
$bulkcopy.bulkcopyTimeout = 0 
$bulkcopy.batchsize = $batchsize 
  
# Create the datatable, and autogenerate the columns. 
$datatable = New-Object System.Data.DataTable 
  
# Open the text file from disk 
$codepage = [System.Text.Encoding]::GetEncoding(1252)
$reader = New-Object System.IO.StreamReader($csvfile, $codepage) 
$columns = (Get-Content $csvfile -First 1).Split($csvdelimiter) 
if ($FirstRowColumnNames -eq $true) { $null = $reader.readLine() } 

$crs = "if object_id('"+$table+"') IS NOT NULL DROP TABLE "+$table
RunSQL($crs)
$crs = ""
$crs = "if object_id('"+$table+"') IS NOT NULL DROP TABLE "+$table+"`nCREATE TABLE "+$table+"(`n"
foreach ($column in $columns) {  
    $crs = $crs + "["+$column+"] nvarchar(max)`n,"
    $null = $datatable.Columns.Add() 
}
$crs = $crs + "`b)"

RunSQL($crs)


  
# Read in the data, line by line, not column by column 
$lineno = 1
while (($line = $reader.ReadLine()) -ne $null)  { 
    try{
    $null = $datatable.Rows.Add($line.Split($csvdelimiter)) 
 
    }
    catch{
        Write-Host $lineno
    }
    $lineno = $lineno + 1
# Import and empty the datatable before it starts taking up too much RAM, but  
# after it has enough rows to make the import efficient. 
    $i++; if (($i % $batchsize) -eq 0) {  
        $bulkcopy.WriteToServer($datatable)  
        Write-Host "$i rows have been inserted in $($elapsed.Elapsed.ToString())." 
        $datatable.Clear()  
    }  
}  
  
# Add in all the remaining rows since the last clear 
if($datatable.Rows.Count -gt 0) { 
    $bulkcopy.WriteToServer($datatable) 
    $datatable.Clear() 
} 
  
# Clean Up 
$reader.Close(); $reader.Dispose() 
$bulkcopy.Close(); $bulkcopy.Dispose() 
$datatable.Dispose() 
  

Write-Host "Script complete. $i rows have been inserted into the database."
Write-Host ""
Write-Host "Rows Loaded:"+$delfile+" "+$i
Write-Host ""
Write-Host "Total Elapsed Time: $($elapsed.Elapsed.ToString())" 
# Sometimes the Garbage Collector takes too long to clear the huge datatable. 
[System.GC]::Collect()